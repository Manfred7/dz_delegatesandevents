﻿using System;
using System.Collections.Generic;

namespace dz_delegatesandevents
{
    internal static class PointListExtentions
    {
        public static T GetMaxElem<T>(this IEnumerable<T> list, Func<T, float> getParametr) where T : class
        {
            
            float maxValue = float.MinValue;
            T maxItem = null;
            float funcVal;
            foreach (var item in list)
            {
                funcVal = getParametr(item);
                if (maxItem != null)
                {
                    if (maxValue < funcVal)
                    {
                        maxValue = funcVal;
                        maxItem = item;
                    }
                }
                else
                {
                    maxItem = item;
                    maxValue = funcVal;
                }
            }
            return maxItem;
        }
    }
}