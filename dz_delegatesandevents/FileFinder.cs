﻿using System;
using System.Collections.Generic;
using System.IO;

namespace dz_delegatesandevents
{
    public class FileArgs : EventArgs
    {
        public FileArgs(string fileName)
        {
            Filename = fileName;
        }
        public string Filename { get; }
        public bool IsTargetFileFound { get; set; }
    }

    public class FileFinder
    {
        public bool NeedStop { get; set; }
        public event EventHandler<FileArgs> OnFileFound;

        public void DoFind(string PathToFolder)
        {
            FileArgs fa = null;
            NeedStop = false;

            IEnumerable<string> allFiles = Directory.EnumerateFiles(PathToFolder);

            foreach (string filename in allFiles)
            {
                fa = new FileArgs(filename);
                OnFileFound?.Invoke(this, fa);
                if (fa.IsTargetFileFound) break;
            }
        }
    }
}