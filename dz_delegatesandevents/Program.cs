﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Linq;

namespace dz_delegatesandevents
{
    internal class Program
    {
        private static List<int> ints = new List<int>() {12, 45, 7, 356};

        private static List<Point> points = new List<Point>()
        {
            new Point() {x = 1, y = 4},
            new Point() {x = 2, y = 11},
            new Point() {x = 2, y = 7},
            new Point() {x = 2, y = 4},
            new Point() {x = 1, y = 7},
            new Point() {x = 1, y = 9},
        };

        private static string StopPattern = "photo";

        public static void Main(string[] args)
        {
            FindMaxPoint();
            FindFiles();
        }

        private static void FileFoundAlarm(object sender, FileArgs fa)
        {
            FileFinder ff;
            ff = (sender as FileFinder);

            Console.WriteLine($"Найден файл:{fa.Filename}");

            if (fa.Filename.Contains(StopPattern))
            {
                fa.IsTargetFileFound = true;
                Console.WriteLine($"Название файла:{fa.Filename} содержит паттерн {StopPattern}, останавливаем поиск!");
            }
        }

        public static void FindFiles()
        {
            string SearchDir = @"C:\Users\Петр\Pictures";
            FileFinder ff = new FileFinder();
            ff.OnFileFound += FileFoundAlarm;
            ff.DoFind(SearchDir);
        }

        public static void FindMaxPoint()
        {
            Point maxPoint = points.GetMaxElem(item => item.x + item.y);
            Console.WriteLine($"Max Point: [{maxPoint.x},{maxPoint.y}] ");
        }
    }
}